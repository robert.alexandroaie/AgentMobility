package agentmobility;

import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;

public class MonitorRefreshBehaviour extends TickerBehaviour {
    
    public MonitorRefreshBehaviour(Agent a, long period) 
    {
        super (a, period);
    }
    
    @Override
    public void onTick()
    {
        MonitorAgent.fc1.DoEvents();
        MonitorAgent.fc2.DoEvents();
    }
}
