package agentmobility;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MyAgent extends Agent {

    public String localhost;

    public void sendToMonitorAgent(String messageContents) {
        ACLMessage m = new ACLMessage();
        m.addReceiver(new AID("MonAgent@" + localhost + ":1090/JADE", AID.ISGUID));
        m.setContent(messageContents);
        send(m);
    }

    @Override
    public void setup() {

        InetAddress[] addr = null;
        try {
            addr = InetAddress.getAllByName(InetAddress.getLocalHost().getHostName());
        } catch (UnknownHostException ex) {
            Logger.getLogger(MyAgent.class.getName()).log(Level.SEVERE, null, ex);
        }
        String[] add = addr[0].toString().split("/");
        localhost = add[1];
        
        sendToMonitorAgent("Created|" + getLocalName() + "|" + here().getName());

        addBehaviour(new MySendBehaviour(this));

        if (this.getLocalName().equals("Agent0")) {
            this.addBehaviour(new MyMoveBehaviour(this, 3000));
        }

        if (this.getLocalName().equals("Agent2")) {
            this.addBehaviour(new MyMoveBehaviour(this, 5000));
        }

    }

    @Override
    public void takeDown() {
        sendToMonitorAgent("Died|" + getLocalName() + "|" + here().getName());
    }

    @Override
    public void beforeMove() {
        sendToMonitorAgent("Moving|" + getLocalName() + "|" + here().getName());
    }

    @Override
    public void afterMove() {
        sendToMonitorAgent("Moved|" + getLocalName() + "|" + here().getName());
    }
}
