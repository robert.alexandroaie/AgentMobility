package agentmobility;

import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.AMSService;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.lang.acl.ACLMessage;
import java.util.LinkedList;

class MySendBehaviour extends OneShotBehaviour {

    private MyAgent myAgent;

    public MySendBehaviour(MyAgent a) {
        super(a);
        myAgent = a;
    }

    @Override
    public void action() {
        try {
            if (myAgent.getLocalName().equals("Agent1")) {
                //send a message to the monitor agent, who is in the same container;                    
                ACLMessage m = new ACLMessage();
                m.addReceiver(new AID("MonAgent", AID.ISLOCALNAME));
                m.setContent("Hello|" + myAgent.getLocalName() + "|" + myAgent.here().getName());
                myAgent.send(m);
            }

            if (myAgent.getLocalName().equals("Agent3")) {
                //send a message to the monitor agent, who is in a different container
                ACLMessage m = new ACLMessage();
                //we need the full newtork name of the agent
                //we need to specify that the name is not local, but a GUID (globally unique ID)
                m.addReceiver(new AID("MonAgent@" + myAgent.localhost + ":1090/JADE", AID.ISGUID));
                m.setContent("Hello|" + myAgent.getLocalName() + "|" + myAgent.here().getName());
                myAgent.send(m);
            }
        } catch (Exception exc) {
            System.out.println(exc.getMessage());
         
        }
    }
}
