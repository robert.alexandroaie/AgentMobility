package agentmobility;

import jade.core.Agent;
import jade.core.ContainerID;
import jade.core.behaviours.TickerBehaviour;

public class MyMoveBehaviour extends TickerBehaviour {
    
    private int _iter = 8;
    
    public MyMoveBehaviour(Agent a, long period) 
    {
        super(a, period);
    }
    
    @Override
    public void onTick()
    {
        if (--_iter > 0)
        {
            if (myAgent.here().getName().equals("Container1M")) {
                myAgent.doMove(new ContainerID("Container2", null));
            }
            else {
                myAgent.doMove(new ContainerID("Container1M", null));
            }
        }
        else {
            block();
        }
    }
}
