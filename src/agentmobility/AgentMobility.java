
package agentmobility;

import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.ControllerException;
import jade.wrapper.StaleProxyException;



public class AgentMobility {

   
    public static void main(String[] args) throws StaleProxyException, InterruptedException, ControllerException 
    {
            AgentContainer container1Main = JadeHelper.CreateContainer("Container1M", true,
                "localhost", "", "1090");
            AgentContainer container2 = JadeHelper.CreateContainer("Container2", false,
                "localhost", "", "1091");
            container1Main.start();
            container2.start();

            Thread.sleep(500);

            AgentController monitorAgent = JadeHelper.CreateAgent(container1Main, 
                "MonAgent", "agentmobility.MonitorAgent", null);
            AgentController agent0 = JadeHelper.CreateAgent(container1Main,
                "Agent0", "agentmobility.MyAgent", null);
            AgentController agent1 = JadeHelper.CreateAgent(container1Main,
                "Agent1", "agentmobility.MyAgent", null);
            AgentController agent2 = JadeHelper.CreateAgent(container2,
                "Agent2", "agentmobility.MyAgent", null);
            AgentController agent3 = JadeHelper.CreateAgent(container2,
                "Agent3", "agentmobility.MyAgent", null);

            monitorAgent.start();

            Thread.sleep(500);

            agent0.start();
            agent1.start();
            agent2.start();
            agent3.start();
    }
}
